## General Information

### Description
<!-- include a description of the change and which issue is fixed -->

### Guide
Please consult this [guide](https://aircall-product.atlassian.net/wiki/spaces/SE/pages/2228588878/YYYY-MM-DD+Release+risk+assessment+template)

### Release Content
- List all components/stacks impacted by the release

### Risk Assesment Checklist
<!-- Chose between LOW, MEDIUM, HIGH -->
Please consult this [template](https://docs.google.com/spreadsheets/d/10hO1d4LRWIYKSOocBgp3zDBxUDXFDTdNKELDC9k7rJQ/template/preview)
- Probability of failure
    - [ ] LOW
    - [ ] MEDIUM
    - [ ] HIGH
- Criticality of features
    - [ ] LOW
    - [ ] MEDIUM
    - [ ] HIGH
- Impacted users
    - [ ] LOW
    - [ ] MEDIUM
    - [ ] HIGH
- Time to recover
    - [ ] LOW
    - [ ] MEDIUM
    - [ ] HIGH
- Risk assessment result 
    - [ ] LOW
    - [ ] MEDIUM
    - [ ] HIGH

### Reviewed by (at least one)
- [ ] Team Lead 
- [ ] Engineering Manager
